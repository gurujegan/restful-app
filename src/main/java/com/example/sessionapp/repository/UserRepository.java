package com.example.sessionapp.repository;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;

import com.example.sessionapp.entity.User;

public interface UserRepository extends JpaRepository<User, Integer> {


	
}
