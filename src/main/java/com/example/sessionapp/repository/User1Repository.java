package com.example.sessionapp.repository;

import org.springframework.stereotype.Repository;

import com.example.sessionapp.entity.User;

import java.lang.Long;

@Repository("user1Repository")
public interface User1Repository extends GenericRepository<User, Long> {

}