package com.example.sessionapp;

import com.example.sessionapp.entity.User;
import com.example.sessionapp.repository.*;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;

public class Test<T>{
	
	T t;
	Integer i;
	

	public Test(T t,Integer i)
	{
		this.t = t;
		this.i = i;
	}

	void show()
	{
	System.out.println(t+" "+i);
	}
	
	public static void main(String arg[])
	{
		Test test = new Test<String>("hi",1);
		test.show();
		
		Test yy = new Test<Integer>(7878,1);
		yy.show();
		

	}
}
