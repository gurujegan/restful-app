package com.example.sessionapp.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class APIController {

	@GetMapping
	@ResponseBody
	public ResponseEntity<?> getLocation() {
		List<String> result = new ArrayList<>();
		result.add("Hello World");
		return new ResponseEntity<>(result,HttpStatus.OK);
	}

	
}
