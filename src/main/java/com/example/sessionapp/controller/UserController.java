package com.example.sessionapp.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.sessionapp.design.BaseRestController;
import com.example.sessionapp.design.RestService;
import com.example.sessionapp.design.RestServiceImplementation;
import com.example.sessionapp.entity.User;
import com.example.sessionapp.repository.GenericRepository;
import com.example.sessionapp.repository.UserRepository;

@RestController
public class UserController extends BaseRestController<User> {
	
	@Autowired
	RestService<User> implementedRestService;

	
	@Override
	protected RestService<User> restService() {
		// TODO Auto-generated method stub
		
		return implementedRestService;
	}

}
