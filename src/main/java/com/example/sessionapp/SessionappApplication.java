package com.example.sessionapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
//@EnableAutoConfiguration 
//@ComponentScan(basePackages={""})
//@EnableJpaRepositories(basePackages="")
//@EnableTransactionManagement @EntityScan(basePackages="")
public class SessionappApplication {

	public static void main(String[] args) {
		SpringApplication.run(SessionappApplication.class, args);
	}

}
