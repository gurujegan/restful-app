package com.example.sessionapp.design;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Optional;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

public abstract class BaseRestController<T> {

	protected static final String BASE_URL = "/api";
	
	protected abstract RestService<T>  restService();

	  @GetMapping("/users")
	  List<T> all() {
	    return restService().findAll();
	  }
	  // end::get-aggregate-root[]

	  @PostMapping("/users")
	  T newEmployee(@RequestBody T t) {
		  System.out.println(t);
	    return restService().save(t);
	  }

	  // Single item
	  
	  @GetMapping("/users/{id}")
	  T one(@PathVariable Integer id) {
	    
	    return restService().findById(id);
	  }
	  
	  // Update item

	  @PutMapping("/users/{id}")
	  public ResponseEntity<T> replaceEmployee(@RequestBody T t, @PathVariable Integer id) throws IllegalAccessException, IllegalArgumentException, InvocationTargetException, NoSuchMethodException, SecurityException {
			Object obj  = restService().findById(id);
		
				Class<?>[] paramTypes = { int.class };
				Object[] args = new Object[1];
		        args[0] = id;
		        
		        t.getClass().getMethod("setId", paramTypes).invoke(t, args);
		        restService().save(t);

			return ResponseEntity.noContent().build();
	  }

	  @DeleteMapping("/users/{id}")
	  void deleteEmployee(@PathVariable Integer id) {
		  restService().delete(id);
	  }
	
	
}
