package com.example.sessionapp.design;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import com.example.sessionapp.entity.User;
import com.example.sessionapp.repository.GenericRepository;
import com.example.sessionapp.repository.UserRepository;


@Service
public class RestServiceImplementation<T> implements RestService<T> {
	
	
	
	
	public enum ENTITIES {User} ;
	
	@Autowired
	JpaRepository<T, Integer> jpaRepo;
	
	@Override
	public T findById(Integer id) {
		// TODO Auto-generated method stub
		return (T) jpaRepo.findById(id);
	}

	@Override
	public T save(T entity) {
		// TODO Auto-generated method stub
		return jpaRepo.save(entity);
	}
	

	@Override
	public List<T> findAll() {
		// TODO Auto-generated method stub
		return (List<T>) jpaRepo.findAll();
	}

	@Override
	public T delete(Integer id) {
		// TODO Auto-generated method stub
		return null;
	}

}
