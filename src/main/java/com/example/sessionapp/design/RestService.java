package com.example.sessionapp.design;

import java.util.List;

import org.springframework.stereotype.Service;


public interface RestService<T> {

	T findById(Integer id);

	T save(T requestBody);

	List<T> findAll();

	//T update(Integer id,T requestBody);

	T delete(Integer id);

}
